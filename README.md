Awesome Color Picker
==================

Chrome Extension allowing you to use a magnifying loupe to find and pick colors in web pages

https://chrome.google.com/webstore/detail/awesome-color-picker/flkdgmgdgnpdecpaaoggnbjcdmbnagbj
