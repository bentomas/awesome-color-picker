var titleStr = 'Awesome Color Picker';

var image = [
    ['#0000ff','#0012ff','#0091ff','#00f3ff','#00ffcd','#00ff6f','#00ff1f'],
    ['#4100ff','#0000ff','#0027ff','#00dcff','#00ffac','#00ff1a','#00ff00'],
    ['#4100ff','#2d00ff','#0700ff','#0073fb','#00ff41','#00ff41','#00ff00'],
    ['#9c00ff','#ac00ff','#c700ff','#e600a3','#d6cf00','#a7ff00','#83ff00'],
    ['#ea00ff','#ff00ff','#ff00e3','#ff003a','#ff4900','#ffea00','#f3ff00'],
    ['#ff00ff','#ff00e4','#ff0090','#ff0000','#ff0000','#ff9500','#ffe400'],
    ['#ff00e3','#ff00ae','#ff005f','#ff0000','#ff0000','#ff5d00','#ffac00']
];

icon128();

function drawIconImage(canvas, context) {
    for (var i = 0; i < image.length; i++) {
        for (var j = 0; j < image[i].length; j++) {
            context.fillStyle = image[j][i];
            console.log(canvas.width/2+i, canvas.height/2+j);
            context.fillRect(canvas.width/2+i-Math.floor(image.length/2), canvas.height/2+j-Math.floor(image[i].length/2),1,1);
        }
    }
}

function icon128() {
    var icon128 = document.getElementById('icon128');
    var canvas = document.createElement('canvas');
    canvas.height = icon128.height;
    canvas.width = icon128.width;
    var context = canvas.getContext('2d');

    var numPixels = 7;
    var pixelSize = 12;

    drawIconImage(canvas, context);

    drawLoupe(context, {x: canvas.width/2, y: canvas.height/2}, {COLOR: false, NUM_PIXELS:numPixels, PIXEL_SIZE:pixelSize}, function() {
        context.beginPath();
        context.arc(canvas.width/2, canvas.height/2, numPixels*pixelSize/2, 0, 2 * Math.PI, false);
        context.lineWidth = 10;
        context.strokeStyle = '#222';
        context.stroke();

        context.strokeStyle = '#000';
        context.lineWidth = 4;
        context.strokeRect(canvas.width/2-pixelSize/2-1.5, canvas.height/2-1.5 - pixelSize/2, pixelSize+3, pixelSize+3);

        //context.strokeStyle = 'blue';
        //context.lineWidth = 1;
        //context.strokeRect(canvas.width/2-48.5, canvas.height/2-48.5, 97, 97);

        icon128.src = canvas.toDataURL('icon128/png');
        icon26.src = icon128.src;

        icon48();
    });
}

function icon48() {
    var icon48 = document.getElementById('icon48');
    var icon26 = document.getElementById('icon26');
    var canvas = document.createElement('canvas');
    canvas.height = icon48.height;
    canvas.width = icon48.width;
    var context = canvas.getContext('2d');

    var numPixels = 5;
    var pixelSize = 8;

    drawIconImage(canvas, context);

    drawLoupe(context, {x: canvas.width/2, y: canvas.height/2}, {COLOR: false, NUM_PIXELS:numPixels, PIXEL_SIZE:pixelSize}, function() {
        context.beginPath();
        context.arc(canvas.width/2, canvas.height/2, numPixels*pixelSize/2, 0, 2 * Math.PI, false);
        context.lineWidth = 4;
        context.strokeStyle = '#222';
        context.stroke();

        context.strokeStyle = '#000';
        context.lineWidth = 1;
        context.strokeRect(canvas.width/2-pixelSize/2-1.5, canvas.height/2-1.5 - pixelSize/2, pixelSize+3, pixelSize+3);

        icon48.src = canvas.toDataURL('image/png');

        promoSmall();
    });
}

function drawImage(context, imageSrc, doubled, cb) {
    if (typeof cb == 'undefined') {
        cb = doubled;
        doubled = undefined;
    }

    var scale = doubled ? 2 : 1;

    var image = new Image();
    image.onload = function() {

        var x = 0;
        while (x < context.canvas.width/scale) {
            y = 0;
            while(y < context.canvas.height/scale) {
                context.drawImage(image, x, y);
                y += this.height;
            }
            x += this.width;
        }

        if (cb) cb();
    }
    image.src = imageSrc;
}

function drawLoupe(context, pos, settings, cb, contextData) {
    var CENTER_PIXEL = Math.floor(settings.NUM_PIXELS/2);
    canvasData = (contextData || context).getImageData(pos.x-CENTER_PIXEL, pos.y-CENTER_PIXEL, settings.NUM_PIXELS, settings.NUM_PIXELS).data;

    var circle = {
        x: pos.x,
        y: pos.y,
        radius: Math.floor(settings.NUM_PIXELS*settings.PIXEL_SIZE/2)
    }

    pos.x = pos.x-Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE;
    pos.y = pos.y-Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE;
    context.save();

    context.beginPath();
    context.arc(circle.x, circle.y, circle.radius, 0, 2 * Math.PI, false);
    context.clip();

    for (x = 0; x < settings.NUM_PIXELS; x++) {
        for (y = 0; y < settings.NUM_PIXELS; y++) {
            index = y*settings.NUM_PIXELS+x;
            index *= 4;

            r = canvasData[index];
            g = canvasData[index+1];
            b = canvasData[index+2];
            context.fillStyle = 'rgb('+r+','+g+','+b+')';
            context.fillRect(circle.x-circle.radius+x*settings.PIXEL_SIZE, circle.y-circle.radius+y*settings.PIXEL_SIZE, settings.PIXEL_SIZE, settings.PIXEL_SIZE);
        }
    }

    index = CENTER_PIXEL*settings.NUM_PIXELS+CENTER_PIXEL;
    index *= 4;
    r = canvasData[index];
    g = canvasData[index+1];
    b = canvasData[index+2];

    context.lineWidth = 1;
    context.strokeStyle = (r+g+b > 384) ? 'rgb(0,0,0)' : 'rgb(255,255,255)';
    context.strokeRect(circle.x-circle.radius+Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE-0.5, circle.y-circle.radius+Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE-0.5, settings.PIXEL_SIZE+1, settings.PIXEL_SIZE+1);

    context.restore();

    context.lineWidth = 2;
    context.beginPath();
    context.strokeStyle = '#222';
    context.arc(circle.x, circle.y, circle.radius+0, 0, 2 * Math.PI, false);
    context.stroke();

    if (typeof settings.COLOR == 'undefined' || settings.color) {
        context.fillStyle = '#333';
        context.lineWidth = 1;
        context.fillRect(circle.x+circle.radius-9, circle.y-21, 21, 41);
        context.strokeRect(circle.x+circle.radius-9+.5, circle.y-21+.5, 20, 41);

        context.font = "normal 9px 'Helvetica'";
        context.textAlign = 'center';
        context.textBaseline = 'middle';
        context.fillStyle = '#fff';
        context.textAlign = 'left';
        context.textBaseline = 'middle';
        context.fillText(strLeftPad(r.toString(16).toUpperCase(),2,0), circle.x+circle.radius-4, circle.y+-12);
        context.fillText(strLeftPad(g.toString(16).toUpperCase(),2,0), circle.x+circle.radius-4, circle.y+0);
        context.fillText(strLeftPad(b.toString(16).toUpperCase(),2,0), circle.x+circle.radius-4, circle.y+12);
    }

    if (cb) cb();
}

function strLeftPad(str, length, padding) {
    str = String(str);
    while(str.length < length) {
        str = padding + str;
    }
    return str;
}

function drawText(context, text, font, color1, color2, doubled, cb) {
    if (typeof cb == 'undefined') {
        cb = doubled;
        doubled = undefined;
    }

    var scale = doubled ? 2 : 1;

    context.font = "normal "+font+"px 'Times New Roman'";
    context.textAlign = 'center';
    context.textBaseline = 'middle';

    context.fillStyle = '#094394';
    context.fillText(titleStr, context.canvas.width/2/scale+1, context.canvas.height/2/scale+1);

    context.fillStyle = '#3182f3';
    context.fillText(titleStr, context.canvas.width/2/scale, context.canvas.height/2/scale);

    if (cb) cb();
}

function promoSmall() {
    var image = document.getElementById('promo-small');
    var canvas = document.createElement('canvas');
    canvas.width = image.width*2;
    canvas.height = image.height*2;
    var context = canvas.getContext('2d');
    context.scale(2,2);

    var canvasSource = document.createElement('canvas');
    canvasSource.width = image.width;
    canvasSource.height = image.height;
    var contextSource = canvasSource.getContext('2d');


    drawImage(context, 'backgrounds/retina_wood/retina_wood.png', true, function() {
        drawImage(contextSource, 'backgrounds/retina_wood/retina_wood.png', function() {
            drawText(context, titleStr, 20, '#094394', '#318f3', true, function() {
                drawText(contextSource, titleStr, 20, '#094394', '#318f3', function() {
                    drawLoupe(context, {x: canvas.width/2*3/4, y: canvas.height/2/2}, {NUM_PIXELS:19, PIXEL_SIZE:4}, function() {
                        image.src = canvas.toDataURL('image/png');
                        promoLarge();
                    }, contextSource);
                });
            });
        });
    });
}
function promoLarge() {
    var image = document.getElementById('promo-large');
    var canvas = document.createElement('canvas');
    canvas.width = image.width*2;
    canvas.height = image.height*2;
    var context = canvas.getContext('2d');
    context.scale(2,2);

    var canvasSource = document.createElement('canvas');
    canvasSource.width = image.width;
    canvasSource.height = image.height;
    var contextSource = canvasSource.getContext('2d');


    drawImage(context, 'backgrounds/retina_wood/retina_wood.png', true, function() {
        drawImage(contextSource, 'backgrounds/retina_wood/retina_wood.png', function() {
            drawText(context, titleStr, 40, '#094394', '#318f3', true, function() {
                drawText(contextSource, titleStr, 40, '#094394', '#318f3', function() {
                    drawLoupe(context, {x: canvas.width/2*3/4, y: canvas.height/2/2}, {NUM_PIXELS:29, PIXEL_SIZE:5}, function() {
                        image.src = canvas.toDataURL('image/png');
                        promoMarquee();
                    }, contextSource);
                });
            });
        });
    });
}
function promoMarquee() {
    var image = document.getElementById('promo-marquee');
    var canvas = document.createElement('canvas');
    canvas.width = image.width*2;
    canvas.height = image.height*2;
    var context = canvas.getContext('2d');
    context.scale(2,2);

    var canvasSource = document.createElement('canvas');
    canvasSource.width = image.width;
    canvasSource.height = image.height;
    var contextSource = canvasSource.getContext('2d');


    drawImage(context, 'backgrounds/retina_wood/retina_wood.png', true, function() {
        drawImage(contextSource, 'backgrounds/retina_wood/retina_wood.png', function() {
            drawText(context, titleStr, 60, '#094394', '#318f3', true, function() {
                drawText(contextSource, titleStr, 60, '#094394', '#318f3', function() {
                    drawLoupe(context, {x: canvas.width/2*3/4, y: canvas.height/2/2}, {NUM_PIXELS:39, PIXEL_SIZE:6}, function() {
                        image.src = canvas.toDataURL('image/png');
                        screenshot1();
                    }, contextSource);
                });
            });
        });
    });
}
function screenshot1() {
    var image = document.getElementById('screenshot1');
    var canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    var context = canvas.getContext('2d');

    drawImage(context, 'backgrounds/retina_wood/retina_wood.png', function() {
        drawText(context, titleStr, 60, '#094394', '#318f3', function() {
            drawLoupe(context, {x: canvas.width/4+4, y: canvas.height/2+6}, {NUM_PIXELS:23, PIXEL_SIZE:4}, function() {
                image.src = canvas.toDataURL('image/png');
                screenshot2();
            });
        });
    });
}
function screenshot2() {
    var image = document.getElementById('screenshot2');
    var canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    var context = canvas.getContext('2d');

    drawImage(context, 'backgrounds/debut_dark/debut_dark.png', function() {
        drawText(context, titleStr, 60, '#094394', '#318f3', function() {
            drawLoupe(context, {x: canvas.width/4*3+60, y: canvas.height/2+6}, {NUM_PIXELS:23, PIXEL_SIZE:4}, function() {
                image.src = canvas.toDataURL('image/png');
                screenshot3();
            });
        });
    });
}
function screenshot3() {
    var image = document.getElementById('screenshot3');
    var canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    var context = canvas.getContext('2d');

    drawImage(context, 'backgrounds/bg1.png', function() {
        drawLoupe(context, {x: canvas.width/2+45, y: canvas.height/2-80}, {NUM_PIXELS:23, PIXEL_SIZE:4}, function() {
            image.src = canvas.toDataURL('image/png');
        });
    });
}
