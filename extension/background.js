/*
 * TODO
 * more specific errors
 * does not work on pages with frames -- show error message
 * when saving NUM_SAVED_COLORS, listen for onunload event
 */

// keep track of state about the current tab
var activatedTab;
var activatedTabError;
// for keeping track of unique activation instances
var activationsCount = 0;

// to dynamically create an icon
var iconCanvas, iconContext;
var lastColor, squareColor;

var POSSIBLE_COLOR_FORMATS = ['rgb', 'rgba', 'hsl', 'hsla', 'hex', '#hex', 'HEX','#HEX'];
var DEFAULT_SETTINGS = {
    COLOR_FORMATS: ['rgb', 'rgba', 'hsl', 'hsla', 'HEX','#HEX'],
    CURRENT_COLOR_FORMAT: 'HEX',
    NUM_PIXELS: 19,             // should be odd
    PIXEL_SIZE: 4,
    NUM_SAVED_COLORS: 15,
}

var settings, savedColors;
loadSyncedData();

chrome.extension.onMessage.addListener(onMessage);
// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(toggleActivated);
// called when a tab is activated (i.e. selected)
chrome.tabs.onActivated.addListener(deactivate);
// called when a tab is updated (like changed away from, or refreshed, or loaded)
chrome.tabs.onUpdated.addListener(deactivate);
// called when a tab is updated (like changed away from, or refreshed, or loaded)
chrome.storage.onChanged.addListener(onChange);

createIcon();

function onMessage(message) {
    switch(message.type) {
        case 'loadScreenshot': 
            loadScreenshot(message.loadId);
            break;

        case 'updateSetting': 
            updateSetting(message.setting, message.value);
            break;

        case 'deactivate': 
            deactivate();
            break;

        case 'saveColor':
            saveColor(message.color, message.colorStr);
            break;

        case 'updateColor':
            if (message.color) lastColor = message.color;
            updateIcon();
            break;
    }
}

function onChange(change, area) {
    for (var key in change) {
        switch (key) {
            case 'savedColors':
                savedColors = typeof change[key].newValue == 'undefined' ? [] : change[key].newValue;
                break;
            default:
                if (key in DEFAULT_SETTINGS) {
                    settings[key] = typeof change[key].newValue == 'undefined' ? DEFAULT_SETTINGS[key] : change[key].newValue;
                    if (activatedTab) {
                        chrome.tabs.sendMessage(activatedTab.id, {type: 'updateSetting', setting: key, value: settings[key]});
                    }
                }
        }
    }
}

function toggleActivated() {
    if (activatedTab != null) deactivate();
    else activate();
}

function activate() {
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
        if (activatedTab != null) { return; }
        if (!settings) {
            tabActivationError('Could not load settings');
            return;
        }
        if (tabs.length < 1 || tabs[0].url.search(/^https?:\/\//) !== 0) {
            tabActivationError('Cannot activate color picker on this web page');
            return;
        }

        activatedTab = {
            id: tabs[0].id,
            windowId: tabs[0].windowId,
            index: activationsCount++,
            url: tabs[0].url,
            title: tabs[0].title,
            incognito: tabs[0].incognito,
        }

        chrome.tabs.executeScript(activatedTab.id, { file: 'page.js' }, function(results) {
            if (results && results.length) {
                chrome.tabs.sendMessage(activatedTab.id, {type: 'activate', settings: settings});
                chrome.browserAction.setTitle({title:'Deactivate color picker'});
                updateIcon();
            }
            else {
                tabActivationError('Cannot activate color picker on this web page');
            }
        });
    });
}

function tabActivationError(e) {
    activatedTabError = e;
    if (activatedTabError) {
        chrome.browserAction.setTitle({title: activatedTabError});
    }
    else {
        chrome.browserAction.setTitle({title:'Activate color picker'});
    }
    activatedTab = null;
    updateIcon();
}

function deactivate() {
    if (activatedTab == null) {
        tabActivationError(null);
        return;
    }

    chrome.tabs.get(activatedTab.id, function(tab) {
        tabActivationError(null);

        if (typeof tab != 'undefined') {
            chrome.tabs.sendMessage(tab.id, {type: 'deactivate'});
        }
    });
}

function loadScreenshot(loadid) {
    if (activatedTab == null) { return; }

    // when a screenshot is received, and we are about to send it back to the tab
    // we want to make sure it is the same tab and activation that requested it,
    // so cache these here...
    var id = activatedTab.id;
    var index = activatedTab.index;

    chrome.tabs.captureVisibleTab(activatedTab.windowId, {format: 'png'}, function(dataUrl) {
        if (!activatedTab || id != activatedTab.id || index != activatedTab.index) { return; }

        chrome.tabs.sendMessage(id, {type: 'screenshot', dataUrl: dataUrl, loadId: loadid});
    });
}

function saveColor(color, colorStr, url, title) {
    var input = document.createElement('input');
    document.body.appendChild(input);
    input.value = colorStr;
    input.select();
    document.execCommand('copy');
    input.parentNode.removeChild(input);

    if (!activatedTab || !activatedTab.incognito) {
        var saving = {
            color: color,
            url: url || activatedTab.url,
            title: title || activatedTab.title,
            time: (new Date()).getTime(),
        }

        for (var i = 0; i < savedColors.length; i++) {
            if (savedColors[i].color[0] == saving.color[0] && savedColors[i].color[1] == saving.color[1] && savedColors[i].color[2] == saving.color[2]) {
                savedColors.splice(i,1);
                i--;
            }
        }

        savedColors.push(saving);

        if (savedColors.length > settings.NUM_SAVED_COLORS) {
            savedColors.shift();
        }

        saveColors();
    }
}

function saveColors() {
    //chrome.storage.sync.remove('savedColors');
    chrome.storage.sync.set({savedColors: savedColors});
}

function loadSyncedData() {
    chrome.storage.sync.get(DEFAULT_SETTINGS, function(obj) {
        settings = obj;
    });
    chrome.storage.sync.get('savedColors', function(colors) {
        savedColors = 'savedColors' in colors ? colors.savedColors : [];
    });
}

function updateSetting(setting, value) {
    //console.log(setting, value);
    switch(setting) {
        case 'NUM_SAVED_COLORS':
            if ((String(value)).search(/^[0-9]+$/) !== 0) {
                value = DEFAULT_SETTINGS.NUM_SAVED_COLORS;
            }
            value = parseInt(value);

            if (savedColors.length > value) {
                savedColors.splice(0, savedColors.length - value);
                saveColors();
            }
            break;
        case 'CURRENT_COLOR_FORMAT':
            if (settings.COLOR_FORMATS.indexOf(value) < 0) {
                // oops, we have to have a color mode has to be one of the ones
                // currently available
                if (POSSIBLE_COLOR_FORMATS.indexOf(value) >= 0) {
                    var colorFormats = [];
                    for (var i = 0; i < POSSIBLE_COLOR_FORMATS.length; i++) {
                        if (settings.COLOR_FORMATS.indexOf(POSSIBLE_COLOR_FORMATS[i]) >= 0 || POSSIBLE_COLOR_FORMATS[i] == value) {
                            colorFormats.push(POSSIBLE_COLOR_FORMATS[i]);
                        }
                    }
                    setTimeout(function() {
                        updateSetting('COLOR_FORMATS', colorFormats);
                    },0);
                }
                else {
                    value = settings.COLOR_FORMATS[0];
                }
            }
            break;
        case 'COLOR_FORMATS':
            if (value.length < 1) {
                // oops, we have to have a color mode
                value = DEFAULT_SETTINGS.COLOR_FORMATS;
            }
            else if (value.indexOf(settings.CURRENT_COLOR_FORMAT) < 0) {
                // after this update is done, update the current mode, to make
                // sure we always have a current mode
                setTimeout(function() {
                    updateSetting('CURRENT_COLOR_FORMAT', settings.COLOR_FORMATS[0]);
                },0);
            }
            break;
    }

    if (DEFAULT_SETTINGS[setting] === value) {
        chrome.storage.sync.remove(setting);
    }
    else {
        var change = {};
        change[setting] = value;
        chrome.storage.sync.set(change);
    }
}

function createIcon() {
    iconCanvas = document.createElement('canvas');
    iconCanvas.width = 19;
    iconCanvas.height = 19;

    document.body.appendChild(iconCanvas);

    iconContext = iconCanvas.getContext('2d');

    updateIcon();
}

function updateColor() {
}

function updateIcon() {
    iconContext.clearRect(0, 0, iconCanvas.width, iconCanvas.height);

    if (activatedTab) {
        if (lastColor) {
            iconContext.fillStyle = 'rgb('+lastColor[0]+','+lastColor[1]+','+lastColor[2]+')';
            squareColor = lastColor[0] + lastColor[1] + lastColor[2] > 384 ? '#000' : '#fff';
        }
        else {
            iconContext.fillStyle = '#FFBF00';
            squareColor = '#000';
        }
        iconContext.beginPath();
        iconContext.arc(9.5, 9.5, 8, 0, 2 * Math.PI, false);
        iconContext.fill();
    }
    else {
        squareColor = '#000';
    }

    iconContext.beginPath();
    iconContext.arc(9.5, 9.5, 8.5, 0, 2 * Math.PI, false);
    iconContext.lineWidth = 2;
    iconContext.strokeStyle = '#333';
    iconContext.stroke();

    iconContext.strokeStyle = squareColor;
    iconContext.lineWidth = 1;
    iconContext.strokeRect(8.5,8.5,2,2);

    if (activatedTabError) {
        iconContext.beginPath();
        iconContext.moveTo(6, 6);
        iconContext.lineTo(13, 13);
        iconContext.moveTo(6, 13);
        iconContext.lineTo(13, 6);
        iconContext.lineWidth = 2;
        iconContext.strokeStyle = '#f00';
        iconContext.stroke();
    }

    chrome.browserAction.setIcon({imageData: iconContext.getImageData(0,0,iconCanvas.width, iconCanvas.height)});
}
