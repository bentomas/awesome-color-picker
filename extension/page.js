if (typeof SimpleColorPickerInitialized == 'undefined') {
    /*
    var varsDiff, varsOld, SimpleColorPickerInitialized, vars = [];
    for (var key in this) {
        vars.push(key);
    }
    */
    SimpleColorPickerInitialized = initialize();
    /*
    setInterval(function() {
        varsOld = vars;
        varsDiff = [];
        vars = [];
        for (var key in this) {
            vars.push(key);
            if (varsOld.indexOf(key) < 0) {
                varsDiff.push(key);
            }
        }
        onsole.log(varsDiff.length);
        onsole.log(varsDiff);

    }, 5000);
    */
}

function initialize() {
    // constants
    var SCROLL_DELAY = 300;                 // amount of time to wait for no scrolling before we assume scrolling is done
    var SCREENSHOT_MAX_TIME = 1000;       // two seconds, how long to wait for a screenshot to come back before giving up
    var LOUPE_BORDER1_COLOR = '#222';
    var LOUPE_BORDER1_WIDTH = 1;
    var LOUPE_BORDER2_COLOR = '#333';
    var LOUPE_BORDER2_WIDTH = 1;
    var DIMENSIONS_SETTINGS = ['NUM_PIXELS','PIXEL_SIZE'];

    // whether or not the loupe is currently activated
    var activated = false;

    // current settings for the loupe
    var settings;

    // save the current color when we draw it
    var currentColor = null, convertedColor, lastColor = null;

    // calculated constants, don't edit these
    var LOUPE_SIZE, LOUPE_HALF_SIZE, PIXEL_HALF_SIZE, CENTER_PIXEL;

    // screenshot details
    var loadedPixels;
    var loading, pendingLoads, loadIds=1;

    // dom/canvas elements
    var container;
    var canvas, canvasContext;
    var loupeDiv, loupeCanvasContainer, loupeCanvas, loupeCanvasContext, colorDetails;

    // onscroll timeout, for only calling screenshots when scrolls finish
    var scrollTimeout;

    // we save the last position of the mouse, so once a screenshot is loaded
    // we can update the loupe div
    var mouseX = 500, mouseY = 500;

    // local variables declared here, so we can reuse them
    var offsetX, offsetY, last, needToLoad, x, y, stopX, stopY, image, canvasData, index, r, g, b, xodd, yodd;

    var hoverInitiateScreenshotTimeout;

    chrome.extension.onMessage.addListener(onMessage);

    return true;

    function calculateConstants() {
        LOUPE_SIZE = settings.NUM_PIXELS*settings.PIXEL_SIZE;
        LOUPE_HALF_SIZE = LOUPE_SIZE/2;
        PIXEL_HALF_SIZE = settings.PIXEL_SIZE/2;
        CENTER_PIXEL = Math.floor(settings.NUM_PIXELS/2);
    }

    function setVariableDimensions() {
        loupeCanvas.width = LOUPE_SIZE;
        loupeCanvas.height = LOUPE_SIZE;
        loupeCanvas.style.border = LOUPE_BORDER2_WIDTH + 'px solid ' + LOUPE_BORDER2_COLOR;
        loupeCanvas.style.borderRadius = (LOUPE_HALF_SIZE+LOUPE_BORDER2_WIDTH)+'px';

        loupeCanvasContainer.style.borderRadius = (LOUPE_HALF_SIZE+LOUPE_BORDER2_WIDTH)+'px';

        colorDetails.style.left = (LOUPE_SIZE+LOUPE_BORDER2_WIDTH-10)+'px';
        colorDetails.style.top = (LOUPE_HALF_SIZE+LOUPE_BORDER2_WIDTH+PIXEL_HALF_SIZE)+'px';

        redrawLoupeDiv();
    }

    function activate(newSettings) {
        settings = newSettings;
        activated = true;

        calculateConstants();

        loadedPixels = [];
        pendingLoads = [];
        image = new Image();
        lastColor = null;

        mouseX = -200;
        mouseY = -200;

        container = document.createElement('div');
        container.style.background = 'none';
        container.style.left = 0;
        container.style.position = 'fixed';
        container.style.top = 0;
        container.style.right = 0;
        container.style.overflow = 'hidden';
        container.style.bottom = 0;
        container.style.zIndex = 20000;

        canvas = document.createElement('canvas');
        //canvas.style.left = '100px';
        //canvas.style.position = 'absolute';
        //canvas.style.top = 0;
        //canvas.style.opacity = .7;
        //canvas.style.zIndex = 19999;
        //document.body.appendChild(canvas);
        canvasContext = canvas.getContext('2d');

        loupeDiv = document.createElement('div');
        loupeDiv.style.left = 0;
        loupeDiv.style.position = 'absolute';
        loupeDiv.style.top = 0;
        loupeDiv.style.zIndex = 20001;
        loupeDiv.style.lineHeight = 0;
        loupeDiv.style.fontSize = 0;

        loupeCanvas = document.createElement('canvas');
        loupeCanvas.style.cursor = 'none';

        loupeCanvasContext = loupeCanvas.getContext('2d');
        loupeCanvasContext.lineWidth = 1;
        loupeCanvasContext.lineJoin = 'miter';

        loupeCanvasContainer = document.createElement('div');
        loupeCanvasContainer.style.background = LOUPE_BORDER2_COLOR;
        loupeCanvasContainer.style.border = LOUPE_BORDER1_WIDTH+'px solid ' + LOUPE_BORDER1_COLOR;
        loupeCanvasContainer.appendChild(loupeCanvas);
        loupeCanvasContainer.style.position = 'absolute';
        loupeCanvasContainer.style.left = 0;
        loupeCanvasContainer.style.top = 0;
        loupeCanvasContainer.style.width = 'auto';

        colorDetails = document.createElement('div');
        colorDetails.style.background = LOUPE_BORDER2_COLOR;
        colorDetails.style.border = LOUPE_BORDER1_WIDTH+'px solid ' + LOUPE_BORDER1_COLOR;
        colorDetails.style.borderRadius = '1px';
        colorDetails.style.color = 'white';
        colorDetails.style.fontSize = '9px';
        colorDetails.style.lineHeight = '12px';
        colorDetails.style.marginTop = '-'+((12*3+4+2)/2+1)+'px';
        colorDetails.style.padding = '2px 4px';
        colorDetails.style.position = 'absolute';
        colorDetails.style.textAlign = 'left';
        colorDetails.style.fontFamily = "'Helvetica', 'Arial', sans-serif";

        setVariableDimensions();

        loupeDiv.appendChild(loupeCanvasContainer);
        loupeDiv.appendChild(colorDetails);

        container.appendChild(loupeDiv);
        document.body.appendChild(container);

        window.addEventListener('scroll',onWindowScroll,false);
        window.addEventListener('resize',onWindowResize,false);
        container.addEventListener('mousemove',onMouseMove,false);
        loupeDiv.addEventListener('mousemove',onMouseMove,false);
        loupeDiv.addEventListener('click',onClick,false);
        document.body.addEventListener('keydown',onKeyDown,false);

        resizeCanvas(document.body.clientWidth, document.body.clientHeight);
    }

    function deactivate() {
        if (activated) {
            activated = false;

            window.removeEventListener('scroll',onWindowScroll,false);
            window.removeEventListener('resize',onWindowResize,false);
            container.removeEventListener('mousemove',onMouseMove,false);
            loupeDiv.removeEventListener('mousemove',onMouseMove,false);
            loupeDiv.removeEventListener('click',onClick,false);
            document.body.removeEventListener('keydown',onKeyDown,false);

            document.body.removeChild(container);
            loadedPixels = [];
            canvasData = loupeDiv = container = canvas = canvasContext = loupeCanvas = loupeCanvasContext = null;
        }
    }

    function onMessage(message) {
        switch(message.type) {
            case 'screenshot':
                screenshotReceived(message.dataUrl, message.loadId);
                break;
            case 'activate':
                activate(message.settings);
                break;
            case 'deactivate':
                deactivate();
                break;
            case 'updateSetting':
                updateSetting(message.setting, message.value);
                break;
        }
    }

    function updateSetting(setting, value) {
        settings[setting] = value;
        if (DIMENSIONS_SETTINGS.indexOf(setting) >= 0) {
            calculateConstants();
            setVariableDimensions();
        }
        updateColorDetails();
    }

    function onWindowResize(e) {
        e.stopPropagation();
        e.preventDefault();

        resizeCanvas(document.body.clientWidth, document.body.clientHeight);

        return false;
    }

    function onWindowScroll(e) {
        e.stopPropagation();
        e.preventDefault();

        if (scrollTimeout) clearTimeout(scrollTimeout);
        scrollTimeout = setTimeout(reloadCurrentView, SCROLL_DELAY);

        return false;
    }

    function onMouseMove(e) {
        e.stopPropagation();
        e.preventDefault();

        mouseX = e.x + document.body.scrollLeft;
        mouseY = e.y + document.body.scrollTop;

        redrawLoupeDiv();

        return false;
    }

    function onClick(e) {
        e.stopPropagation();
        e.preventDefault();

        if (currentColor) {
            chrome.extension.sendMessage(null,{ type: 'saveColor', color: currentColor, colorStr: getColorStr()});
        }
        initiateDeactivation();

        return false;
    }

    function initiateDeactivation() {
        deactivate();
        chrome.extension.sendMessage(null,{ type: 'deactivate' });
    }

    function onKeyDown(e) {
        //console.log(e.keyCode);
        switch(e.keyCode) {
            case 27: // escape key
                initiateDeactivation();
                break;
            case 32: // space bar
                index = settings.COLOR_FORMATS.indexOf(settings.CURRENT_COLOR_FORMAT);
                index = index < 0 ? 0 : (index+1) % settings.COLOR_FORMATS.length;
                chrome.extension.sendMessage(null,{ type: 'updateSetting', setting: 'CURRENT_COLOR_FORMAT', value: settings.COLOR_FORMATS[index] });
                e.preventDefault();
                break;
            case 189: // minus/underscore key
                n = Math.max(5,settings.NUM_PIXELS-2);
                chrome.extension.sendMessage(null,{ type: 'updateSetting', setting: 'NUM_PIXELS', value: n });
                break;
            case 187: // plus/equal key
                n = settings.NUM_PIXELS+2;
                if (checkDimensions(settings.PIXEL_SIZE, n)) {
                    chrome.extension.sendMessage(null,{ type: 'updateSetting', setting: 'NUM_PIXELS', value: n });
                }
                break;
            case 49:  // 1-9
            case 50:  // 2 key
            case 51:  // 3 key
            case 52:  // 4 key
            case 53:  // 5 key
            case 54:  // 6 key
            case 55:  // 7 key
            case 56:  // 8 key
            case 57:  // 9 key
                n = e.keyCode-48;
                if (checkDimensions(n, settings.NUM_PIXELS)) {
                    chrome.extension.sendMessage(null,{ type: 'updateSetting', setting: 'PIXEL_SIZE', value: n });
                }
                break;
        }

        e.stopPropagation();
        return false;
    }

    function checkDimensions(pixelSize, numPixels) {
        return (pixelSize*numPixels < 250);
    }

    function resizeCanvas(width, height) {
        canvas.width = width;
        canvas.height = height;

        for (x = 0; x < width; x++) {
            loadedPixels[x] = [];
            for (y = 0; y < height; y++) {
                loadedPixels[x][y] = 0;
            }
        }

        setTimeout(reloadCurrentView,0);
    }

    function reloadCurrentView() {
        if (hoverInitiateScreenshotTimeout) {
            clearTimeout(hoverInitiateScreenshotTimeout);
            hoverInitiateScreenshotTimeout = null;
        }
        offsetX = document.body.scrollLeft;
        offsetY = document.body.scrollTop;

        needToLoad = false;
        for (x = offsetX, stopX = offsetX+window.innerWidth; x < stopX && !needToLoad; x++) {
            if (!(x in loadedPixels)) {
                needToLoad = true;
            }
            for (y = offsetY, stopY = offsetY+window.innerHeight; y < stopY && !needToLoad; y++) {
                if (!(y in loadedPixels[x]) || !loadedPixels[x][y]) {
                    needToLoad = true;
                }
            }
        }
        if (needToLoad) {
            loadScreenshot({offsetX:offsetX, offsetY:offsetY});
        }
    }

    function loadScreenshot(details) {
        if (loading) {
            pendingLoads.push({offsetX:offsetX, offsetY:offsetY});
            return;
        }

        if (typeof details == 'undefined') {
            details = pendingLoads.shift();
        }

        if (!details) {
            return;
        }

        loupeDiv.style.marginLeft = '-2000px';

        loading = {
            timeout: setTimeout(screenshotReceived,SCREENSHOT_MAX_TIME),
            loadId: loadIds++,
            offsetX: details.offsetX,
            offsetY: details.offsetY
        };

        setTimeout(sendLoadScreenshotMessage,10);
    }

    function sendLoadScreenshotMessage() {
        chrome.extension.sendMessage(null,{ type: 'loadScreenshot', loadId: loading.loadId });
    }

    function screenshotReceived(dataUrl, loadId) {
        last = loading;

        loading = null;
        loupeDiv.style.marginLeft = 0;
        if (pendingLoads.length>0) { setTimeout(loadScreenshot, 10); }

        if (typeof dataUrl == 'undefined' || typeof loadId == 'undefined' || !last || last.loadId != loadId) {
            //console.log('failed screenshot', loadId);
            return;
        }

        clearTimeout(last.timeout);

        image.setAttribute('data-offsetX', last.offsetX);
        image.setAttribute('data-offsetY', last.offsetY);
        image.onload = screenshotLoaded;
        image.src = dataUrl;
    }

    function screenshotLoaded() {
        offsetX = parseInt(this.getAttribute('data-offsetX'));
        offsetY = parseInt(this.getAttribute('data-offsetY'));

        if (offsetX+this.width > canvas.width && offsetY+this.height > canvas.height) {
            resizeCanvas(offsetX + this.width, offsetY + this.height);
        }
        else if (offsetX+this.width > canvas.width) {
            resizeCanvas(offsetX + this.width, canvas.height);
        }
        else if (offsetY+this.height > canvas.height) {
            resizeCanvas(canvas.width, offsetY+this.height);
        }

        for (x = offsetX, stopX = offsetX+this.width; x < stopX; x++) {
            if (!(x in loadedPixels)) { loadedPixels[x] = []; }
            for (y = offsetY, stopY = offsetY+this.height; y < stopY; y++) {
                loadedPixels[x][y] = 1;
            }
        }

        canvasContext.drawImage(this, offsetX, offsetY);

        redrawLoupeDiv();
    }

    function redrawLoupeDiv() {
        loupeDiv.style.left = (mouseX-LOUPE_HALF_SIZE-PIXEL_HALF_SIZE-document.body.scrollLeft)+'px';
        loupeDiv.style.top = (mouseY-LOUPE_HALF_SIZE-PIXEL_HALF_SIZE-document.body.scrollTop)+'px';

        canvasData = canvasContext.getImageData(mouseX-CENTER_PIXEL, mouseY-CENTER_PIXEL, settings.NUM_PIXELS, settings.NUM_PIXELS).data;

        if (!(mouseX in loadedPixels) || !(mouseY in loadedPixels[mouseX]) || !loadedPixels[mouseX][mouseY]) {
            if (!hoverInitiateScreenshotTimeout) {
                hoverInitiateScreenshotTimeout = setTimeout(reloadCurrentView, SCREENSHOT_MAX_TIME);
            }
            for (x = 0; x < settings.NUM_PIXELS/2; x++) {
                for (y = 0; y < settings.NUM_PIXELS/2; y++) {
                    index = y*settings.NUM_PIXELS/2+x;
                    index *= 4;

                    xodd = x%2 == 1;
                    yodd = y%2 == 1;
                    if ((xodd && yodd) || (!xodd && !yodd)) {
                        r = 200;
                        g = 200;
                        b = 200;
                    }
                    else {
                        r = 255;
                        g = 255;
                        b = 255;
                    }
                    loupeCanvasContext.fillStyle = 'rgba('+r+','+g+','+b+',1)';
                    loupeCanvasContext.fillRect(x*settings.PIXEL_SIZE*2, y*settings.PIXEL_SIZE*2, settings.PIXEL_SIZE*2, settings.PIXEL_SIZE*2);
                }
            }

            updateColor(null);
        }
        else {
            if (hoverInitiateScreenshotTimeout) {
                clearTimeout(hoverInitiateScreenshotTimeout);
                hoverInitiateScreenshotTimeout = null;
            }
            for (x = 0; x < settings.NUM_PIXELS; x++) {
                for (y = 0; y < settings.NUM_PIXELS; y++) {
                    index = y*settings.NUM_PIXELS+x;
                    index *= 4;

                    r = canvasData[index];
                    g = canvasData[index+1];
                    b = canvasData[index+2];
                    loupeCanvasContext.fillStyle = 'rgb('+r+','+g+','+b+')';
                    loupeCanvasContext.fillRect(x*settings.PIXEL_SIZE, y*settings.PIXEL_SIZE, settings.PIXEL_SIZE, settings.PIXEL_SIZE);
                }
            }

            index = CENTER_PIXEL*settings.NUM_PIXELS+CENTER_PIXEL;
            index *= 4;
            r = canvasData[index];
            g = canvasData[index+1];
            b = canvasData[index+2];

            updateColor([r,g,b]);
        }

        loupeCanvasContext.strokeStyle = (r+g+b > 384) ? 'rgb(0,0,0)' : 'rgb(255,255,255)';
        loupeCanvasContext.strokeRect(Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE-.5, Math.floor(settings.NUM_PIXELS/2)*settings.PIXEL_SIZE-.5, settings.PIXEL_SIZE+1, settings.PIXEL_SIZE+1);
    }

    function updateColor(color) {
        lastColor = currentColor;
        currentColor = color;
        if (currentColor != null && (lastColor == null || currentColor[0] != lastColor[0] || currentColor[1] != lastColor[1] || currentColor[2] != lastColor[2])) {
            chrome.extension.sendMessage(null, {type: 'updateColor', color: currentColor});
        }
        updateColorDetails();
    }

    function updateColorDetails() {
        if (currentColor) {
            switch(settings.CURRENT_COLOR_FORMAT) {
                case 'rgb':
                    colorDetails.innerHTML = currentColor[0] + '<br>' + currentColor[1] + '<br>' + currentColor[2];
                    colorDetails.style.width = '16px';
                    break;
                case 'rgba':
                    colorDetails.innerHTML = currentColor[0] + '<br>' + currentColor[1] + '<br>' + currentColor[2] + '<br>1';
                    colorDetails.style.width = '16px';
                    break;
                case 'hsl':
                    convertedColor = rgbToHsl(currentColor);
                    colorDetails.innerHTML = convertedColor[0] + '°<br>' + convertedColor[1] + '%<br>' + convertedColor[2] + '%';
                    colorDetails.style.width = '22px';
                    break;
                case 'hsla':
                    convertedColor = rgbToHsl(currentColor);
                    colorDetails.innerHTML = convertedColor[0] + '°<br>' + convertedColor[1] + '%<br>' + convertedColor[2] + '%<br>1';
                    colorDetails.style.width = '22px';
                    break;
                case 'HEX':
                    convertedColor = rgbToHex(currentColor);
                    colorDetails.innerHTML = (convertedColor[0] + '<br>' + convertedColor[1] + '<br>' + convertedColor[2]).toUpperCase();
                    colorDetails.style.width = '12px';
                    break;
                case 'hex':
                    convertedColor = rgbToHex(currentColor);
                    colorDetails.innerHTML = convertedColor[0] + '<br>' + convertedColor[1] + '<br>' + convertedColor[2];
                    colorDetails.style.width = '12px';
                    break;
                case '#HEX':
                    convertedColor = rgbToHex(currentColor);
                    colorDetails.innerHTML = '#<br>' + (convertedColor[0] + '<br>' + convertedColor[1] + '<br>' + convertedColor[2]).toUpperCase();
                    colorDetails.style.width = '11px';
                    break;
                case '#hex':
                    convertedColor = rgbToHex(currentColor);
                    colorDetails.innerHTML = '#<br>' + convertedColor[0] + '<br>' + convertedColor[1] + '<br>' + convertedColor[2];
                    colorDetails.style.width = '11px';
                    break;
            }
        }
        else {
            switch(settings.CURRENT_COLOR_FORMAT) {
                case 'rgb':
                case 'rgba':
                    colorDetails.innerHTML = '<br><br><br>';
                    colorDetails.style.width = '16px';
                    break;
                case 'hsl':
                case 'hsla':
                    colorDetails.innerHTML = '<br><br><br>';
                    colorDetails.style.width = '22px';
                    break;
                case '#HEX':
                case '#hex':
                    colorDetails.innerHTML = '<br><br><br><br>';
                    colorDetails.style.width = '11px';
                    break;
                case 'HEX':
                case 'hex':
                    colorDetails.innerHTML = '<br><br><br>';
                    colorDetails.style.width = '11px';
                    break;
            }
        }
        colorDetails.style.marginTop = '-'+(colorDetails.offsetHeight/2+LOUPE_BORDER1_WIDTH)+'px';
    }

    function getColorStr() {
        if (currentColor) {
            switch(settings.CURRENT_COLOR_FORMAT) {
                case 'rgb':
                    return 'rgb('+currentColor[0] + ',' + currentColor[1] + ',' + currentColor[2]+')';
                    break;
                case 'rgba':
                    return 'rgba('+currentColor[0] + ',' + currentColor[1] + ',' + currentColor[2]+',1)';
                    break;
                case 'hsl':
                    convertedColor = rgbToHsl(currentColor);
                    return 'hsl('+convertedColor[0] + ',' + convertedColor[1] + '%,' + convertedColor[2] + '%)';
                    break;
                case 'hsla':
                    convertedColor = rgbToHsl(currentColor);
                    return 'hsla('+convertedColor[0] + ',' + convertedColor[1] + '%,' + convertedColor[2] + '%,1)';
                    break;
                case 'HEX':
                    convertedColor = rgbToHex(currentColor);
                    return (convertedColor[0] + convertedColor[1] + convertedColor[2]).toUpperCase();
                    break;
                case 'hex':
                    convertedColor = rgbToHex(currentColor);
                    return convertedColor[0] + convertedColor[1] + convertedColor[2];
                    break;
                case '#HEX':
                    convertedColor = rgbToHex(currentColor);
                    return '#' + (convertedColor[0] + convertedColor[1] + convertedColor[2]).toUpperCase();
                    break;
                case '#hex':
                    convertedColor = rgbToHex(currentColor);
                    return '#' + convertedColor[0] + convertedColor[1] + convertedColor[2];
                    break;
            }
        }
        else {
            return '';
        }
    }

    function strLeftPad(str, length, padding) {
        str = String(str);
        while(str.length < length) {
            str = padding + str;
        }
        return str;
    }

    /**
    * Converts an RGB color value to HSL. Conversion formula
    * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
    * Assumes r, g, and b are contained in the set [0, 255] and
    * returns h, s, and l in the set [0, 1].
    *
    * http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
    */
    function rgbToHsl(color){
        r = color[0]/255, g = color[1]/255, b = color[2]/255;
        var max = Math.max(r, g, b), min = Math.min(r, g, b);
        var h, s, l = (max + min) / 2;

        if(max == min){
            h = s = 0; // achromatic
        }else{
            var d = max - min;
            s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
            switch(max){
                case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                case g: h = (b - r) / d + 2; break;
                case b: h = (r - g) / d + 4; break;
            }
            h /= 6;
        }

        return [Math.round(h*360), Math.round(s*100), Math.round(l*100)];
    }

    function rgbToHex(color) {
        return [strLeftPad(color[0].toString(16),2,0), strLeftPad(color[1].toString(16),2,0), strLeftPad(color[2].toString(16),2,0)];
    }
}
