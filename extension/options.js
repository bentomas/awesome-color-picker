var background = chrome.extension.getBackgroundPage();

var savedColorsUl = document.getElementById('saved-colors');
var numColorsInput = document.getElementById('num-saved-colors');

var changeFunctions = {
    'savedColors': loadColors,
    'COLOR_FORMATS': updateAvailableColorModes,
    'CURRENT_COLOR_FORMAT': updateCurrentColorMode,
    'NUM_SAVED_COLORS': updateNumSavedColors,
}

chrome.storage.onChanged.addListener(onChange);
window.onunload = numColorsInput.onchange = numColorsInput.onblur = numColorsInput.onkeyup = saveNumColors;
savedColorsUl.onclick = saveColor;
setInterval(updateTimes,30*1000);


createColorFormatsTable();
loadColors(background.savedColors);
updateAvailableColorModes(background.settings.COLOR_FORMATS);
updateCurrentColorMode(background.settings.CURRENT_COLOR_FORMAT);
updateNumSavedColors(background.settings.NUM_SAVED_COLORS);

function loadColors(colors) {
    while (savedColorsUl.childNodes.length) {
        savedColorsUl.removeChild(savedColorsUl.childNodes[0]);
    }
    if (colors.length < 1) {
        document.getElementById('recent-colors-section').style.display = 'none';
    }
    else {
        document.getElementById('recent-colors-section').style.display = 'block';
        for (var i = colors.length-1; i >= 0; i--) {
            savedColorsUl.appendChild(createColorLi(colors[i]));
        }
        updateTimes();
    }
};

function onChange(change, area) {
    for (var key in change) {
        if (key in changeFunctions) {
            changeFunctions[key](typeof change[key].newValue == 'undefined' ? background.DEFAULT_SETTINGS[key] : change[key].newValue);
        }
    }
}

function saveNumColors(e) {
    if (typeof e.keyCode == 'undefined' || e.keyCode == 13) {
        background.updateSetting('NUM_SAVED_COLORS', numColorsInput.value);
    }
}

function saveColor(e) {
    var color = e.target.parentNode.getAttribute('data-color').split(',');
    var colorStr = getColorStr(color);
    var link = e.target.parentNode.getElementsByTagName('a')[0];
    var url = link.href;
    var title = link.innerHTML;

    background.saveColor(color, colorStr, url, title);
}

function updateCurrentColorMode(currentColorMode) {
    for (var i = 0; i < background.POSSIBLE_COLOR_FORMATS.length; i++) {
        var currentInput = document.getElementById('current-'+i);
        currentInput.checked = background.POSSIBLE_COLOR_FORMATS[i] == currentColorMode ? 'checked' : false;
    }

    var colorStrs = document.getElementsByClassName('color-string');
    for (var i = 0; i < colorStrs.length; i++) {
        colorStrs[i].innerHTML = getColorStr(colorStrs[i].parentNode.getAttribute('data-color').split(','));
    }
}

function updateAvailableColorModes(colorModes) {
    for (var i = 0; i < background.POSSIBLE_COLOR_FORMATS.length; i++) {
        var activeInput = document.getElementById('active-'+i);
        var index = colorModes.indexOf(background.POSSIBLE_COLOR_FORMATS[i]);
        activeInput.checked = index >= 0 ? 'checked' : false;
    }
}

function updateNumSavedColors(numSavedColors) {
    numColorsInput.value = numSavedColors;
}

function createColorFormatsTable() {
    var table = document.getElementById('color-formats');
    var color = [255,255,255];
    for (var i = 0; i < background.POSSIBLE_COLOR_FORMATS.length; i++) {
        var tr = document.createElement('tr');
        tr.innerHTML = '<td><label for="active-'+i+'">'+getColorStr(color, background.POSSIBLE_COLOR_FORMATS[i])+'</label></td>'
                     + '<td><input name="active" id="active-'+i+'" type="checkbox"></td>'
                     + '<td><input name="current" id="current-'+i+'" type="radio"></td>'
                     ;

        var tds = tr.getElementsByTagName('td');

        tds[1].getElementsByTagName('input')[0].onclick = saveColorFormats;
        tds[2].getElementsByTagName('input')[0].onclick = saveCurrentColorFormat;

        table.appendChild(tr);
    }
}

function saveColorFormats() {
    var colorModes = [];
    for (var i = 0; i < background.POSSIBLE_COLOR_FORMATS.length; i++) {
        var activeInput = document.getElementById('active-'+i);
        if (activeInput.checked) {
            colorModes.push(background.POSSIBLE_COLOR_FORMATS[i]);
        }
    }
    background.updateSetting('COLOR_FORMATS', colorModes);
}

function saveCurrentColorFormat() {
    var currentColorFormat = null;
    for (var i = 0; i < background.POSSIBLE_COLOR_FORMATS.length; i++) {
        var currentInput = document.getElementById('current-'+i);
        if (currentInput.checked) {
            currentColorFormat = background.POSSIBLE_COLOR_FORMATS[i];
        }
    }

    if (currentColorFormat != 'null') {
        background.updateSetting('CURRENT_COLOR_FORMAT', currentColorFormat);
    }
}

function createColorLi(color) {
    var li = document.createElement('li');
    li.id = (rgbToHex(color.color)).join('');
    li.setAttribute('data-color', color.color.join(','));
    li.innerHTML = '<span title="Copy this color to your clipboard" class="color" style="background: '+getColorStr(color.color, '#hex')+';"></span>'
                    + '<span class="color-string">'+getColorStr(color.color)+'</span>'
                    + '<a href="'+color.url+'" title="'+color.title+'" class="source">'+color.title+'</a>'
                    + '<span class="time" data-time="'+color.time+'"></span>'
                    ;
    return li;
}

function updateTimes() {
    var spans = document.getElementsByClassName('time');
    for (var i = 0; i < spans.length; i++) {
        var time = moment(parseInt(spans[i].getAttribute('data-time'))).fromNow();
        spans[i].innerHTML = time;
    }
}

function getColorStr(color, format) {
    if (color) {
        switch(format || background.settings.CURRENT_COLOR_FORMAT) {
            case 'rgb':
                return 'rgb('+color[0] + ',' + color[1] + ',' + color[2]+')';
                break;
            case 'rgba':
                return 'rgba('+color[0] + ',' + color[1] + ',' + color[2]+',1)';
                break;
            case 'hsl':
                convertedColor = rgbToHsl(color);
                return 'hsl('+convertedColor[0] + ',' + convertedColor[1] + '%,' + convertedColor[2] + '%)';
                break;
            case 'hsla':
                convertedColor = rgbToHsl(color);
                return 'hsla('+convertedColor[0] + ',' + convertedColor[1] + '%,' + convertedColor[2] + '%,1)';
                break;
            case 'HEX':
                convertedColor = rgbToHex(color);
                return (convertedColor[0] + convertedColor[1] + convertedColor[2]).toUpperCase();
                break;
            case 'hex':
                convertedColor = rgbToHex(color);
                return convertedColor[0] + convertedColor[1] + convertedColor[2];
                break;
            case '#HEX':
                convertedColor = rgbToHex(color);
                return '#' + (convertedColor[0] + convertedColor[1] + convertedColor[2]).toUpperCase();
                break;
            case '#hex':
                convertedColor = rgbToHex(color);
                return '#' + convertedColor[0] + convertedColor[1] + convertedColor[2];
                break;
        }
    }
    else {
        return '';
    }
}

function strLeftPad(str, length, padding) {
    str = String(str);
    while(str.length < length) {
        str = padding + str;
    }
    return str;
}

/**
* Converts an RGB color value to HSL. Conversion formula
* adapted from http://en.wikipedia.org/wiki/HSL_color_space.
* Assumes r, g, and b are contained in the set [0, 255] and
* returns h, s, and l in the set [0, 1].
*
* http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
*/
function rgbToHsl(color){
    r = parseInt(color[0])/255, g = parseInt(color[1])/255, b = parseInt(color[2])/255;
    var max = Math.max(r, g, b), min = Math.min(r, g, b);
    var h, s, l = (max + min) / 2;

    if(max == min){
        h = s = 0; // achromatic
    }else{
        var d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
        switch(max){
            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
            case g: h = (b - r) / d + 2; break;
            case b: h = (r - g) / d + 4; break;
        }
        h /= 6;
    }

    return [Math.round(h*360), Math.round(s*100), Math.round(l*100)];
}

function rgbToHex(color) {
    return [strLeftPad(parseInt(color[0]).toString(16),2,0), strLeftPad(parseInt(color[1]).toString(16),2,0), strLeftPad(parseInt(color[2]).toString(16),2,0)];
}
